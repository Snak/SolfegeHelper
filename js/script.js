Array.prototype.sample = function() {
    return this[Math.floor(Math.random()*this.length)];
}

function getObjectValue(object, propertyName) {
    return object[propertyName];
};

let good_count = 0;
let bad_count = 0;
let notes_sol = {
    B: ['B', 'SI', 'シ', 'し'],
    C: ['C', 'DO', 'ド', 'ど'],
    D: ['D', 'RE', 'レ', 'れ'],
    E: ['E', 'MI', 'ミ', 'み'],
    F: ['F', 'FA', 'ファ', 'ふぁ'],
    G: ['G', 'SOL', 'ソ', 'そ'],
    A: ['A', 'LA', 'ラ', 'ら'],
    H: ['B', 'SI', 'シ', 'し'],
    I: ['C', 'DO', 'ド', 'ど'],
    J: ['D', 'RE', 'レ', 'れ'],
    K: ['E', 'MI', 'ミ', 'み'],
};

let notes_fa = {
    B: ['D', 'RE', 'レ', 'れ'],
    C: ['E', 'MI', 'ミ', 'み'],
    D: ['F', 'FA', 'ファ', 'ふぁ'],
    E: ['G', 'SOL', 'ソ', 'そ'],
    F: ['A', 'LA', 'ラ', 'ら'],
    G: ['B', 'SI', 'シ', 'し'],
    A: ['C', 'DO', 'ド', 'ど'],
    H: ['D', 'RE', 'レ', 'れ'],
    I: ['E', 'MI', 'ミ', 'み'],
    J: ['F', 'FA', 'ファ', 'ふぁ'],
    K: ['G', 'SOL', 'ソ', 'そ'],
};

function setNewNote() {
    document.getElementById('answer').innerHTML = Object.keys(notes_sol).sample();
}

function switchKey(radio) {
    if (radio.value === 'sol') {
        // '!' is the hymnus font's G-clef symbole 
        document.getElementById('key').innerHTML = '!';
    } else {
        // '?' is F-clef.
        document.getElementById('key').innerHTML = '?';
    }
    setNewNote()
}

function check(input) {
    let value = input.value.toUpperCase();
    let answer = document.getElementById('answer').innerHTML.toUpperCase();
    let notes = (document.getElementById('fa-radio').checked) ? notes_fa : notes_sol;

    if (getObjectValue(notes, answer).indexOf(value) >= 0) {
        document.getElementById('goods').innerHTML = ++good_count;
        setNewNote();
    } else {
        document.getElementById('bads').innerHTML = ++bad_count;
    }

    input.value = '';
}

window.onload = function() {
    document.getElementById('sol-radio').checked = true;
    setNewNote();
};